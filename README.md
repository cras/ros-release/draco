## draco (noetic) - 1.5.6-3

The packages in the `draco` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic --new-track draco` on `Thu, 25 May 2023 18:31:39 -0000`

The `draco` package was released.

Version of package(s) in repository `draco`:

- upstream repository: https://github.com/google/draco
- release repository: unknown
- rosdistro version: `null`
- old version: `1.5.6-2`
- new version: `1.5.6-3`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## draco (melodic) - 1.3.6-3

The packages in the `draco` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic draco` on `Thu, 11 May 2023 03:05:01 -0000`

The `draco` package was released.

Version of package(s) in repository `draco`:

- upstream repository: https://github.com/google/draco
- release repository: unknown
- rosdistro version: `null`
- old version: `1.3.6-2`
- new version: `1.3.6-3`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


